/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
 * For this class I used code from http://codingexodus.blogspot.co.uk/2012/12/working-with-video-using-opencv-and-qt.html
 *
*/

#ifndef RBANNOTATOR_H
#define RBANNOTATOR_H

#include <QtWidgets>
#include <QPair>
#include "annotationworker.h"
#include "player.h"
#include "progressdialog.h"

QT_BEGIN_NAMESPACE
class QAbstractButton;
class QSlider;
class QLabel;
QT_END_NAMESPACE

class RBAnnotator : public QWidget {
    Q_OBJECT
public:
    RBAnnotator(QWidget *parent = 0);
    ~RBAnnotator();

    bool exportAnnotationTextFile(QString outputFile);
    
public slots:
    void updateProgressDialog(QString message, int progress);
    void incrementProgressDialog(QString message);

private slots:
    void positionChanged(int position);
    void setPosition(int position);
    void currentFrameEdited();
    void currentTimeEdited();
    void deleteAnnotationClicked();
    void userStartedMovingSlider();
    void userStoppedMovingSlider();
    void loadVideo();
    void play();
    void newAnnotationClicked();  
    void exportAnnotationsClicked();
    void cutVideo();
    void updatePlayerUI(QImage img);
    void oneFrameForwards();
    void oneFrameBackwards();
    void tenFramesBackwards();
    void tenFramesForwards();
    void rotate180degreesChecked();
    void hideCutVideoSettingsDialog();
    void resizeSliders();
    void markButtonClicked();
    void jumpToAnnotation();
    void loadAnnotationsClicked();

private:
    QLabel * videoFrame;
    Player* opencvPlayer;
    QAbstractButton
    *playButton, *oneFrameBackwardsButton, *oneFrameForwardsButton, *tenFramesBackwardsButton, *tenFramesForwardsButton,
    *newAnnotationButton, *openButton, *exportButton, *cutVideoButton, *loadAnnotationsButton;
    QSlider *positionSlider;
    QBoxLayout * annotationsLayout;
    QWidget * currentAnnotationPane;
    QVector<QWidget*> annotationPanesList;
    QScrollArea * scrollArea;
    QPushButton * markButton;
    QLineEdit * currentFrameLineEdit, * currentTimeLineEdit;
    QString currentVideoName;
    QLabel * videoNameLabel;
    QCheckBox * rotate180degreesCheckBox;
    QThread * workerThread;
    AnnotationWorker * annotationWorker;
    QDialog * cutVideoSettingsDialog;
    ProgressDialog * progressDialog;
    int currentMarkingState;
    int addAnnotationState;
    bool updateAnnotations();
    bool isAnnotationValid(const Annotation *a);

    static const double SLIDER_ADJUST_RATIO_START = 0.009852476;
    static const double SLIDER_ADJUST_RATIO_END =0.009799789;
    static const int MARKING_PRE_START = 1;
    static const int MARKING_PRE_END = 2;
    static const int MARKING_ACT_START = 3;
    static const int MARKING_ACT_END = 4;
    static const int MARKING_POST_START = 5;
    static const int MARKING_POST_END = 6;
    static const int ADD_NEW_ANNOTATION = 1;
    static const int FINALISE_ANNOTATION = 2;

    QBoxLayout * setControlLayout();
    QBoxLayout * createControlLayout();
    QDialog * createCutVideoSettingsDialog();
    void reset();
    void deleteAnnotation(QWidget* toBeDeletedPane);
    void finaliseAnnotation();
    bool userMovingSlider;
    void setVideoPlayer();
    void enableMediaButtons(bool enable);
    bool rotate180degrees;
    bool videoWasPlayingBeforeMovingSlider;
    void getCutVideoSettings(bool * useOriginalCodec, int * frameWidth, int * frameHeight);
    void setWorker();
    QWidget * createNewAnnotationPane(int n);
    void setPositionSlider();
    void setAnnotationScrollArea();   
    void setPreActionalSliderStart(double adjustFactor, QSlider* preactionalSlider, Annotation* a);
    void setPreActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* preactionalSlider, Annotation* a);
    void setActionalSliderStart(double adjustFactor, QSlider* actionalSlider, Annotation* a);
    void setActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* actionalSlider, Annotation* a);
    void setPostActionalSliderStart(double adjustFactor, QSlider* postactionalSlider, Annotation* a);
    void setPostActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* postactionalSlider, Annotation* a);

    void deleteAllAnnotations();

signals:
    void cutVideo(cv::VideoCapture *inputVideo, QString outputFolder, bool useOriginalCodec, bool resize, int frameWidth, int frameHeight);

protected:
    void closeEvent (QCloseEvent *event);
    bool eventFilter(QObject *object, QEvent *event);
    void resizeEvent(QResizeEvent * event);
};

#endif

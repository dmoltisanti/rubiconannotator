/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
*/

#ifndef ANNOTATIONWORKER_H
#define ANNOTATIONWORKER_H

#include "annotation.h"
#include <opencv2/opencv.hpp>
#include <QObject>
#include <QVector>
#include <QPair>
#include <QString>

class AnnotationWorker : public QObject {
    Q_OBJECT

public:
    AnnotationWorker();

public:
    QVector<Annotation *> getAnnotations() const;
    void newAnnotation();
    int getPreActionalStart();
    int getPreActionalEnd();
    int getActionalStart();
    int getActionalEnd();
    int getPostActionalStart();
    int getPostActionalEnd();
    void setPreActionalStart(int start);
    void setPreActionalEnd(int end);
    void setActionalStart(int start);
    void setActionalEnd(int end);
    void setPostActionalStart(int start);
    void setPostActionalEnd(int end);
    void setVerb(QString verb);
    void setObjects(QString objects);
    void setVideoName(QString videoName);
    void finaliseAnnotation();
    void deleteAnnotation(int index);
    int getNumberOfAnnotations();
    bool isAnnotating() const;
    void stopAnnotating();
    Annotation *getCurrentAnnotation() const;
    void updateVerbsAndObjects(QVector<QPair<QString, QString> > verbsAndObjects);
    void setAnnotations(QVector<Annotation *> &value);

public slots:
    void cutVideo(cv::VideoCapture *inputVideo, QString outputFolder, bool useOriginalCodec, bool resize, int frameWidth, int frameHeight);

signals:
    void incrementProgress(QString message);
    void setProgress(QString message, int value);

private:
    Annotation * currentAnnotation;
    QVector<Annotation*> annotations;
    bool annotating;
    void writeSegmentToVideo(cv::VideoCapture *inputVideo, cv::VideoWriter *outputVideo, int startFrame, int endFrame, bool resizeFrame, cv::Size S);
};

#endif // ANNOTATIONWORKER_H

/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
*/

#ifndef ANNOTATION_H
#define ANNOTATION_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QTextStream>
#include <QFile>
#include <QDir>

class Annotation {

public:
    Annotation();
    int getStartFrame_pre() const;
    void setStartFrame_pre(int value);
    int getEndFrame_pre() const;
    void setEndFrame_pre(int value);
    int getStartFrame_act() const;
    void setStartFrame_act(int value);
    int getEndFrame_act() const;
    void setEndFrame_act(int value);
    int getStartFrame_post() const;
    void setStartFrame_post(int value);
    int getEndFrame_post() const;
    void setEndFrame_post(int value);
    QString getVerb();
    void setVerb(QString value);
    QStringList getObjects() const;
    void setObjects(const QStringList &value);
    void setObjects(QString value);
    QString getVideoName() const;
    void setVideoName(const QString &value);
    QString objectsToString();
    QString formatAnnotation();
    QString buildFileName(int whichOne);

    static QVector<Annotation*> loadAnnotationsFromFile(QFile &annotationFile, QVector<int> &badAnnotations);
    static Annotation * parseAnnotationFromString(QString &text);
    static bool exportAnnotationsToFile(QFile &outputFile, QVector<Annotation*> annotations);
    static bool isAnnotationValid(const Annotation *a);

    static const int ALL=0;
    static const int PRE=1;
    static const int ACT=2;
    static const int POST=3;
    static const int PRE_ACT=4;
    static const int ACT_POST=5;

    static bool compareAnnotations(const Annotation* a, const Annotation* b) {
        return a->getStartFrame_pre() < b->getStartFrame_pre();
    }

private:
    int startFrame_pre, endFrame_pre, startFrame_act, endFrame_act, startFrame_post, endFrame_post;
    QString verb, videoName;
    QStringList objects;
};

#endif // ANNOTATION_H

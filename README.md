# Rubicon Annotator #

![Screenshot from 2017-03-01 12-25-39.png](https://bitbucket.org/repo/7dboa7/images/1049156190-Screenshot%20from%202017-03-01%2012-25-39.png)


The Rubicon Annotator is a video annotating software written in C++.

### Dependencies ###

* OpenCV compiled with ffmpeg. Version should not matter;
* Qt5 (potentially could work with Qt4 as well, never tried though);
* CMake.

### How do I get set up? ###

* cmake .
* make
* ./RBAnnotator

### What can I do with this? ###

* Label multiple actions in a video with a verb and one or more objects;
* For each action you can mark three different sub-segments, called the *pre-actional* phase, the *actional* phase and the *post-actional* phase. The three sub-segments are forced to be contiguous in time;
* Export annotations as a text file;
* Load annotations from a text file;
* Cut the video using the marked segments. For each labelled action it will create several videos containing the three sub-segments alone, as well as the concatenation of them.

### Keyboard shortcuts ###

* Arrow up: skip one frame forwards;
* Arrow down: skip one frame backwards;
* Page up: skip ten frames forwards;
* Page down: skip ten frames backwards;
* F1: play/pause the video;
* F2: mark beginning/ending of action segments;
* F3: create/finalise annotation.

### Annotation format ###

The annotations file will contain one row per labelled action. 

The annotation format is **verb_objects_preStart-preEnd_actStart-actEnd_postStart-postEnd**

Multiple objects will be separated with '+'. Spaces will be replaced with '-'. Marked start/end times are printed as frames. 

Example: pour_bowl+water_1022-1043_1044-1148_1149-1186

* verb: *pour*
* objects: *bowl*, *water*
* pre-actional start frame: *1022*
* pre-actional end frame: *1043*
* actional start frame: *1044*
* actional end frame: *1148*
* post-actional start frame: *1149*
* post-actional end frame: *1186*

### Acknowledgements ###

I used some existing code to implement the OpenCV/Qt interface. [Check out the original code here](https://codingexodus.blogspot.co.uk/2012/12/working-with-video-using-opencv-and-qt.html).

### Contact me ###

davide (dot) moltisanti (at) bristol (dot) ac (dot) uk
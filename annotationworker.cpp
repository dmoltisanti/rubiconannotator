/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
*/

#include "annotationworker.h"
#include <QDebug>

AnnotationWorker::AnnotationWorker() {
    annotating = true;
}

QVector<Annotation *> AnnotationWorker::getAnnotations() const {
    return annotations;
}

void AnnotationWorker::newAnnotation() {
    currentAnnotation = new Annotation();
    annotating = true;
}

int AnnotationWorker::getPreActionalStart() {
    return currentAnnotation->getStartFrame_pre();
}

int AnnotationWorker::getPreActionalEnd() {
    return currentAnnotation->getEndFrame_pre();
}

int AnnotationWorker::getActionalStart() {
    return currentAnnotation->getStartFrame_act();
}

int AnnotationWorker::getActionalEnd() {
    return currentAnnotation->getEndFrame_act();
}

int AnnotationWorker::getPostActionalStart() {
    return currentAnnotation->getStartFrame_post();
}

int AnnotationWorker::getPostActionalEnd() {
    return currentAnnotation->getEndFrame_post();
}

void AnnotationWorker::setPreActionalStart(int start) {
    currentAnnotation->setStartFrame_pre(start);
}

void AnnotationWorker::setPreActionalEnd(int end) {
    currentAnnotation->setEndFrame_pre(end);
}

void AnnotationWorker::setActionalStart(int start) {
    currentAnnotation->setStartFrame_act(start);
}

void AnnotationWorker::setActionalEnd(int end) {
    currentAnnotation->setEndFrame_act(end);
}

void AnnotationWorker::setPostActionalStart(int start) {
    currentAnnotation->setStartFrame_post(start);
}

void AnnotationWorker::setPostActionalEnd(int end) {
    currentAnnotation->setEndFrame_post(end);
}

void AnnotationWorker::setVerb(QString verb) {
    currentAnnotation->setVerb(verb);
}

void AnnotationWorker::setObjects(QString objects) {
    currentAnnotation->setObjects(objects);
}

void AnnotationWorker::setVideoName(QString videoName) {
    currentAnnotation->setVideoName(videoName);
}

void AnnotationWorker::finaliseAnnotation() {
    annotations.push_back(currentAnnotation);
    annotating = false;
}

void AnnotationWorker::deleteAnnotation(int index) {
    if(index < annotations.size() && index >= 0) {
        Annotation * a = annotations[index];
        annotations.remove(index);
    }
}

int AnnotationWorker::getNumberOfAnnotations() {
    return annotations.size();
}

bool AnnotationWorker::isAnnotating() const {
    return annotating;
}

void AnnotationWorker::stopAnnotating() {
    annotating = false;
}

void AnnotationWorker::cutVideo(cv::VideoCapture *inputVideo, QString outputFolder, bool useOriginalCodec, bool resize, int frameWidth, int frameHeight) {
    int ex = useOriginalCodec ? static_cast<int>(inputVideo->get(CV_CAP_PROP_FOURCC)) : CV_FOURCC('M','J','P','G');
    cv::Size S = cv::Size(frameWidth, frameHeight);
    int count = 0;

    for(int i=0; i<annotations.size(); i++) {
        // writing all
        QString allFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::ALL) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter allVideo(allFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &allVideo, annotations[i]->getStartFrame_pre(), annotations[i]->getEndFrame_pre(), resize, S); // writing pre-actional phase
        writeSegmentToVideo(inputVideo, &allVideo, annotations[i]->getStartFrame_act(), annotations[i]->getEndFrame_act(), resize, S); // writing actional phase
        writeSegmentToVideo(inputVideo, &allVideo, annotations[i]->getStartFrame_post(), annotations[i]->getEndFrame_post(), resize, S); // writing post-actional phase
        allVideo.release();

        // writing pre
        QString preFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::PRE) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter preVideo(preFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &preVideo, annotations[i]->getStartFrame_pre(), annotations[i]->getEndFrame_pre(), resize, S); // writing pre-actional phase
        preVideo.release();

        //writing act
        QString actFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::ACT) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter actVideo(actFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &actVideo, annotations[i]->getStartFrame_act(), annotations[i]->getEndFrame_act(), resize, S); // writing actional phase
        actVideo.release();

        //writing post
        QString postFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::POST) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter postVideo(postFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &postVideo, annotations[i]->getStartFrame_post(), annotations[i]->getEndFrame_post(), resize, S); // writing post-actional phase

        // writing pre-act
        QString preActFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::PRE_ACT) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter preActVideo(preActFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &preActVideo, annotations[i]->getStartFrame_pre(), annotations[i]->getEndFrame_pre(), resize, S); // writing pre-actional phase
        writeSegmentToVideo(inputVideo, &preActVideo, annotations[i]->getStartFrame_act(), annotations[i]->getEndFrame_act(), resize, S); // writing actional phase
        preActVideo.release();

        // writing act-post
        QString actPostFilePath = outputFolder + QDir::separator() + annotations[i]->buildFileName(Annotation::ACT_POST) + ".avi";
        emit incrementProgress(QString("Cutting video %1/%2...").arg(++count).arg(annotations.size()*6));
        cv::VideoWriter actPostVideo(actPostFilePath.toStdString(), ex, inputVideo->get(CV_CAP_PROP_FPS), S, true);
        writeSegmentToVideo(inputVideo, &actPostVideo, annotations[i]->getStartFrame_act(), annotations[i]->getEndFrame_act(), resize, S); // writing actional phase
        writeSegmentToVideo(inputVideo, &actPostVideo, annotations[i]->getStartFrame_post(), annotations[i]->getEndFrame_post(), resize, S); // writing post-actional phase
        actPostVideo.release();
    }
}

void AnnotationWorker::setAnnotations(QVector<Annotation *> &value) {
    annotations = value;
}

Annotation *AnnotationWorker::getCurrentAnnotation() const {
    return currentAnnotation;
}

void AnnotationWorker::updateVerbsAndObjects(QVector<QPair<QString, QString> > verbsAndObjects) {
    for(int i=0; i<verbsAndObjects.size(); i++) {
        if(i < this->annotations.size()) {
            annotations[i]->setVerb(verbsAndObjects.at(i).first);
            annotations[i]->setObjects(verbsAndObjects.at(i).second);
        }
    }
}

void AnnotationWorker::writeSegmentToVideo(cv::VideoCapture *inputVideo, cv::VideoWriter *outputVideo, int startFrame, int endFrame, bool resizeFrame, cv::Size S) {
    inputVideo->set(CV_CAP_PROP_POS_FRAMES, startFrame);
    cv::Mat src;
    int currentFrame = startFrame;

    while(true) {
        inputVideo->read(src);

        if(src.empty() || currentFrame > endFrame || currentFrame > inputVideo->get(CV_CAP_PROP_FRAME_COUNT)-1)
            break;

        if(resizeFrame && S.width > 0 && S.height > 0)
            cv::resize(src, src, S);

        outputVideo->write(src);
        currentFrame++;
    }
}

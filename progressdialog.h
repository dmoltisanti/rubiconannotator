/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
 * For this class I used code from http://codingexodus.blogspot.co.uk/2012/12/working-with-video-using-opencv-and-qt.html
 *
*/

#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>
#include <QProgressBar>
#include <QLayout>
#include <QLabel>

class ProgressDialog : public QDialog {
    Q_OBJECT

public:
    ~ProgressDialog();

    static ProgressDialog* getProgressDialog(QWidget * parent, const QString & title, const int & maxValue);

public:
    void setProgressValue(const int &value);
    void setProgressValue(const int &value, const QString &message);
    void incrementValue();
    void incrementValue(QString message);
    void setMessage(const QString &message);
    void reset(const QString &newTitle, const int &newMaxValue);
    bool hasCompleted();

private:
    explicit ProgressDialog(QWidget *parent = 0);
    QProgressBar * progressBar;
    QLabel * label;
};

#endif // PROGRESSDIALOG_H

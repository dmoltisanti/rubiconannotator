/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
*/

#include "rbannotator.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    RBAnnotator annotator;
    annotator.showMaximized();

    return app.exec();
}

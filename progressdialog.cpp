/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
 * For this class I used code from http://codingexodus.blogspot.co.uk/2012/12/working-with-video-using-opencv-and-qt.html
 *
*/

#include "progressdialog.h"

ProgressDialog::ProgressDialog(QWidget *parent) {
    QBoxLayout * layout = new QVBoxLayout;
    progressBar = new QProgressBar;
    label = new QLabel;
    label->setAlignment(Qt::AlignCenter);
    layout->addWidget(progressBar);
    layout->addWidget(label);
    this->setLayout(layout);
}

ProgressDialog::~ProgressDialog() {

}

ProgressDialog *ProgressDialog::getProgressDialog(QWidget *parent, const QString &title, const int &maxValue) {
    ProgressDialog* progressDialog = new ProgressDialog(parent);
    progressDialog->setWindowTitle(title);
    progressDialog->progressBar->setMaximum(maxValue);
    progressDialog->progressBar->setValue(0);
    progressDialog->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    progressDialog->setCursor(Qt::WaitCursor);
    progressDialog->setSizeGripEnabled(false); 
    return progressDialog;
}

void ProgressDialog::setProgressValue(const int &value) {
    progressBar->setValue(value);
    progressBar->update();
}

void ProgressDialog::setProgressValue(const int &value, const QString &message) {
    progressBar->setValue(value);
    label->setText(message);
    progressBar->update();
}

void ProgressDialog::incrementValue() {
    this->setProgressValue(progressBar->value()+1);
}

void ProgressDialog::incrementValue(QString message) {
    this->incrementValue();
    this->setMessage(message);
}

void ProgressDialog::setMessage(const QString &message) {        
    label->setText(message);
}

void ProgressDialog::reset(const QString &newTitle, const int &newMaxValue) {
    this->setWindowTitle(newTitle);
    progressBar->setMaximum(newMaxValue);
    this->setProgressValue(0);
    this->setMessage("");
    this->update();
    this->adjustSize();
    this->adjustPosition(this->parentWidget());
}

bool ProgressDialog::hasCompleted() {
    return this->progressBar->value() >= this->progressBar->maximum();
}

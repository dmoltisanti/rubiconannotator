/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
 * For this class I used code from http://codingexodus.blogspot.co.uk/2012/12/working-with-video-using-opencv-and-qt.html
 *
*/

#include "rbannotator.h"

RBAnnotator::RBAnnotator(QWidget *parent) : QWidget(parent), playButton(0), positionSlider(0) {
    QBoxLayout *controlLayout = createControlLayout();
    setPositionSlider();
    setAnnotationScrollArea();

    videoNameLabel = new QLabel();
    videoNameLabel->setMaximumHeight(20);

    videoFrame = new QLabel();
    setVideoPlayer();

    QBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(videoNameLabel);
    mainLayout->addWidget(videoFrame);
    mainLayout->addLayout(controlLayout);
    mainLayout->addWidget(positionSlider);
    mainLayout->addWidget(scrollArea);

    setLayout(mainLayout);

    this->setMinimumWidth(1200);
    this->setMinimumHeight(900);

    userMovingSlider = false;
    rotate180degrees = false;
    videoWasPlayingBeforeMovingSlider = false;
    currentMarkingState = MARKING_PRE_START;
    addAnnotationState = ADD_NEW_ANNOTATION;

    setWorker();
    progressDialog = ProgressDialog::getProgressDialog(this, "Progress", 100);
    this->installEventFilter(this);
}

void RBAnnotator::setWorker() {
    workerThread = new QThread(this);
    annotationWorker = new AnnotationWorker();
    annotationWorker->moveToThread(workerThread);
    connect(workerThread, SIGNAL(finished()), annotationWorker, SLOT(deleteLater()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(this, SIGNAL(cutVideo(cv::VideoCapture*,QString,bool,bool,int,int)), annotationWorker, SLOT(cutVideo(cv::VideoCapture*,QString,bool,bool,int,int)));
    connect(annotationWorker, SIGNAL(incrementProgress(QString)), this, SLOT(incrementProgressDialog(QString)));
    connect(annotationWorker, SIGNAL(setProgress(QString,int)), this, SLOT(updateProgressDialog(QString,int)));
    workerThread->start();
}

QBoxLayout * RBAnnotator::createControlLayout() {
    openButton = new QPushButton(tr("Open..."));
    connect(openButton, SIGNAL(clicked()), this, SLOT(loadVideo()));

    loadAnnotationsButton = new QPushButton("Load annotations");
    loadAnnotationsButton->setEnabled(false);
    connect(loadAnnotationsButton, SIGNAL(clicked()), this, SLOT(loadAnnotationsClicked()));

    playButton = new QPushButton;
    playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    playButton->setToolTip("F1");
    connect(playButton, SIGNAL(clicked()), this, SLOT(play()));

    tenFramesBackwardsButton = new QPushButton;
    tenFramesBackwardsButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    tenFramesBackwardsButton->setAutoRepeat(true);
    tenFramesBackwardsButton->setToolTip("Page Down");
    connect(tenFramesBackwardsButton, SIGNAL(clicked()), SLOT(tenFramesBackwards()));

    oneFrameBackwardsButton = new QPushButton;
    oneFrameBackwardsButton->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    oneFrameBackwardsButton->setAutoRepeat(true);
    oneFrameBackwardsButton->setToolTip("Down arrow");
    connect(oneFrameBackwardsButton, SIGNAL(clicked()), SLOT(oneFrameBackwards()));

    oneFrameForwardsButton = new QPushButton;
    oneFrameForwardsButton->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    oneFrameForwardsButton->setAutoRepeat(true);
    oneFrameForwardsButton->setToolTip("Up arrow");
    connect(oneFrameForwardsButton, SIGNAL(clicked()), SLOT(oneFrameForwards()));

    tenFramesForwardsButton = new QPushButton;
    tenFramesForwardsButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    tenFramesForwardsButton->setAutoRepeat(true);
    tenFramesForwardsButton->setToolTip("Page Up");
    connect(tenFramesForwardsButton, SIGNAL(clicked()), SLOT(tenFramesForwards()));

    newAnnotationButton = new QPushButton;
    newAnnotationButton->setEnabled(false);
    newAnnotationButton->setText("New annotation");
    newAnnotationButton->setToolTip("F3");
    connect(newAnnotationButton, SIGNAL(clicked()), this, SLOT(newAnnotationClicked()));

    markButton = new QPushButton("Start pre-actional");
    markButton->setStyleSheet("color: blue;");
    markButton->setToolTip("F2");
    markButton->setEnabled(false);

    connect(markButton, SIGNAL(clicked()), this, SLOT(markButtonClicked()));

    exportButton = new QPushButton("Save annotations");
    exportButton->setEnabled(false);
    connect(exportButton, SIGNAL(clicked()), this, SLOT(exportAnnotationsClicked()));

    cutVideoButton = new QPushButton("Cut video");
    cutVideoButton->setEnabled(false);
    connect(cutVideoButton, SIGNAL(clicked()), this, SLOT(cutVideo()));

    QLabel * currentFrameLabel = new QLabel("Frame");
    QLabel * currentTimeLabel = new QLabel("Time");
    currentFrameLineEdit = new QLineEdit("0");
    currentTimeLineEdit = new QLineEdit("00:00:00:000");
    currentFrameLineEdit->setToolTip("Insert a frame and press Enter to jump throughout the video");
    currentTimeLineEdit->setToolTip("Insert a time and press Enter to jump throughout the video. Expected format is hh:mm:ss:zzz");
    currentFrameLineEdit->setStyleSheet("color: white; background-color: black; border: 0px");
    currentTimeLineEdit->setStyleSheet("color: white; background-color: black; border: 0px");
    currentFrameLineEdit->setMaximumWidth(110);
    currentTimeLineEdit->setMaximumWidth(110);
    currentFrameLineEdit->setValidator(new QIntValidator);
    currentFrameLineEdit->setAlignment((Qt::AlignRight));
    currentFrameLineEdit->setEnabled(false);
    currentTimeLineEdit->setEnabled(false);
    connect(currentTimeLineEdit, SIGNAL(returnPressed()), this, SLOT(currentTimeEdited()));
    connect(currentFrameLineEdit, SIGNAL(returnPressed()), this, SLOT(currentFrameEdited()));

    QRegExp rx("\\d{1,2}:\\d{1,2}:\\d{1,2}:\\d{1,3}");
    currentTimeLineEdit->setValidator(new QRegExpValidator(rx));
    currentTimeLineEdit->setAlignment(Qt::AlignRight);

    QFrame* vLine = new QFrame;
    vLine->setFrameShape(QFrame::VLine);
    QFrame* vLine2 = new QFrame;
    vLine2->setFrameShape(QFrame::VLine);
    QFrame* vLine3 = new QFrame;
    vLine3->setFrameShape(QFrame::VLine);

    rotate180degreesCheckBox = new QCheckBox("Rotate 180°");
    connect(rotate180degreesCheckBox, SIGNAL(stateChanged(int)), this, SLOT(rotate180degreesChecked()));

    QBoxLayout *controlLayout = new QHBoxLayout;
    controlLayout->setMargin(0);
    controlLayout->addWidget(openButton);
    controlLayout->addWidget(loadAnnotationsButton);
    controlLayout->addWidget(exportButton);
    controlLayout->addWidget(cutVideoButton);
    controlLayout->addWidget(vLine);
    controlLayout->addWidget(tenFramesBackwardsButton);
    controlLayout->addWidget(oneFrameBackwardsButton);
    controlLayout->addWidget(playButton);
    controlLayout->addWidget(oneFrameForwardsButton);
    controlLayout->addWidget(tenFramesForwardsButton);
    controlLayout->addWidget(vLine2);
    controlLayout->addWidget(newAnnotationButton);
    controlLayout->addWidget(markButton);
    controlLayout->addWidget(vLine3);
    controlLayout->addWidget(rotate180degreesCheckBox);
    controlLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    controlLayout->addWidget(currentFrameLabel);
    controlLayout->addWidget(currentFrameLineEdit);
    controlLayout->addWidget(currentTimeLabel);
    controlLayout->addWidget(currentTimeLineEdit);

    enableMediaButtons(false);

    return controlLayout;
}

QDialog *RBAnnotator::createCutVideoSettingsDialog() {
    QDialog * dialog = new QDialog;
    QBoxLayout * layout = new QVBoxLayout;
    int frameWidth, frameHeight;
    opencvPlayer->getFrameSize(&frameWidth, &frameHeight);

    QBoxLayout * topLayout = new QHBoxLayout;

    QCheckBox * useOriginalVideoFormatCheckBox = new QCheckBox("Use original video codec (if not checked will use MJPEG)");
    useOriginalVideoFormatCheckBox->setObjectName("formatCheckBox");
    topLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    topLayout->addWidget(useOriginalVideoFormatCheckBox);

    QBoxLayout * centralLayout = new QHBoxLayout;

    QLabel * frameWidthLabel = new QLabel("Frame width");
    QLabel * frameHeightLabel = new QLabel("Frame height");
    QLineEdit * frameWidthLineEdit = new QLineEdit(QString::number(frameWidth));
    QLineEdit * frameHeightLineEdit = new QLineEdit(QString::number(frameHeight));
    frameWidthLineEdit->setAlignment(Qt::AlignRight);
    frameWidthLineEdit->setValidator(new QIntValidator);
    frameWidthLineEdit->setObjectName("frameWidthLineEdit");
    frameHeightLineEdit->setValidator(new QIntValidator);
    frameHeightLineEdit->setAlignment(Qt::AlignRight);
    frameHeightLineEdit->setObjectName("frameHeightLineEdit");

    centralLayout->addWidget(frameWidthLabel);
    centralLayout->addWidget(frameWidthLineEdit);
    centralLayout->addWidget(frameHeightLabel);
    centralLayout->addWidget(frameHeightLineEdit);

    QPushButton * okButton = new QPushButton("Ok");
    connect(okButton, SIGNAL(clicked()), this, SLOT(hideCutVideoSettingsDialog()));

    QBoxLayout * bottomLayout = new QHBoxLayout;
    bottomLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    bottomLayout->addWidget(okButton);

    layout->addLayout(topLayout);
    layout->addLayout(centralLayout);
    layout->addLayout(bottomLayout);

    dialog->setWindowTitle("Video cutting settings");
    dialog->setLayout(layout);

    return dialog;
}

void RBAnnotator::setPositionSlider() {
    positionSlider = new QSlider(Qt::Horizontal);
    positionSlider->setRange(0, 0); // will be changed when loading the video
    positionSlider->setStyleSheet("QSlider::groove:horizontal {background: black; margin-left:18%; margin-right:18%} QSlider::handle:horizontal {width: 20px; background: #ffcc00;}");

    connect(positionSlider, SIGNAL(valueChanged(int)), this, SLOT(positionChanged(int)));
    connect(positionSlider, SIGNAL(sliderMoved(int)), this, SLOT(setPosition(int)));
    connect(positionSlider, SIGNAL(sliderPressed()), this, SLOT(userStartedMovingSlider()));
    connect(positionSlider, SIGNAL(sliderReleased()), this, SLOT(userStoppedMovingSlider()));
}

void RBAnnotator::setAnnotationScrollArea() {
    scrollArea = new QScrollArea;
    scrollArea->setMaximumHeight(250);
    scrollArea->setBackgroundRole(QPalette::Window);
    scrollArea->setFrameShadow(QFrame::Plain);
    scrollArea->setFrameShape(QFrame::NoFrame);
    scrollArea->setWidgetResizable(true);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QWidget * scrollAreaInner = new QWidget(scrollArea);
    annotationsLayout = new QVBoxLayout(scrollAreaInner);
    scrollAreaInner->setLayout(annotationsLayout);
    scrollArea->setWidget(scrollAreaInner);
}

bool RBAnnotator::eventFilter(QObject *object, QEvent *event) {
    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

    if(keyEvent->type() != QEvent::KeyRelease) {
        return false;
    }

    int pressedKey = keyEvent->key();

    switch(pressedKey) {
    case Qt::Key_F1:
        videoFrame->setFocus();
        this->play();
        return true;
    case Qt::Key_PageUp:
        videoFrame->setFocus();
        this->tenFramesForwards();
        return true;
    case Qt::Key_PageDown:
        videoFrame->setFocus();
        this->tenFramesBackwards();
        return true;
    case Qt::Key_Down:
        videoFrame->setFocus();
        this->oneFrameBackwards();
        return true;
    case Qt::Key_Up:
        videoFrame->setFocus();
        this->oneFrameForwards();
        return true;
    case Qt::Key_F3:
        if(newAnnotationButton->isEnabled()) {
            newAnnotationClicked();
            return true;
        }
    case Qt::Key_F2:
        if(markButton->isEnabled()) {
            markButtonClicked();
            return true;
        }
    default:
        return false;
    }

    return false;
}

void RBAnnotator::resizeEvent(QResizeEvent *event) {
    resizeSliders();
}

void RBAnnotator::updateProgressDialog(QString message, int progress) {
    if(message.isEmpty())
        progressDialog->setProgressValue(progress);
    else
        progressDialog->setProgressValue(progress, message);

    if(progressDialog->hasCompleted()) {
        progressDialog->hide();
        QMessageBox messageBox;
        messageBox.information(0,"","Done!");
    }
}

void RBAnnotator::incrementProgressDialog(QString message) {
    if(message.isEmpty())
        progressDialog->incrementValue();
    else
        progressDialog->incrementValue(message);

    if(progressDialog->hasCompleted()) {
        progressDialog->hide();
        QMessageBox messageBox;
        messageBox.information(0,"","Done!");
    }
}

void RBAnnotator::updatePlayerUI(QImage img) {
    if (!img.isNull()) {
        videoFrame->setAlignment(Qt::AlignCenter);
        QPixmap pixMap = QPixmap::fromImage(img).scaled(videoFrame->size(), Qt::KeepAspectRatio, Qt::FastTransformation);

        if(rotate180degrees) {
            QMatrix rm;
            pixMap = pixMap.transformed(rm.rotate(180));
        }

        videoFrame->setPixmap(pixMap);
        int position = opencvPlayer->getCurrentFramePosition();

        if(!userMovingSlider && !opencvPlayer->isStopped())
            positionSlider->setValue(position); // this will call setPosition()
    }
}

void RBAnnotator::oneFrameForwards() {
    if(! opencvPlayer->isVideoLoaded())
        return;

    positionSlider->setValue(positionSlider->value()+1);
}

void RBAnnotator::oneFrameBackwards() {
    if(! opencvPlayer->isVideoLoaded())
        return;

    positionSlider->setValue(positionSlider->value()-1);
}

void RBAnnotator::tenFramesBackwards() {
    if(! opencvPlayer->isVideoLoaded())
        return;

    positionSlider->setValue(positionSlider->value()-10);
}

void RBAnnotator::tenFramesForwards() {
    if(! opencvPlayer->isVideoLoaded())
        return;

    positionSlider->setValue(positionSlider->value()+10);
}

void RBAnnotator::rotate180degreesChecked() {
    rotate180degrees = rotate180degreesCheckBox->isChecked();

    if(opencvPlayer->isStopped()) {
        updatePlayerUI(opencvPlayer->getCurrentFrameImg());
    }
}

void RBAnnotator::hideCutVideoSettingsDialog() {
    cutVideoSettingsDialog->hide();
}

void RBAnnotator::setPreActionalSliderStart(double adjustFactor, QSlider* preactionalSlider, Annotation* a) {
    if(a->getStartFrame_pre() > 0) {
        int marginLeft =  round(( (a->getStartFrame_pre() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactor);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: blue; margin-left: %1%}").arg(marginLeft);
        preactionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::setPreActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* preactionalSlider, Annotation* a) {
    if(a->getEndFrame_pre() > 0) {
        int marginLeft =  round(( (a->getStartFrame_pre() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactorStart);
        int marginRight = round( (100 - ( (a->getEndFrame_pre() / (double) opencvPlayer->getNumberOfFrames()) * 100)) * adjustFactorEnd);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: blue; margin-left: %1%; margin-right: %2%}").arg(marginLeft).arg(marginRight);
        preactionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::setActionalSliderStart(double adjustFactor, QSlider* actionalSlider, Annotation* a) {
    if(a->getStartFrame_act() > 0) {
        int marginLeft =  round(( (a->getEndFrame_pre() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactor);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: red; margin-left: %1%}").arg(marginLeft);
        actionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::setActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* actionalSlider, Annotation* a) {
    if(a->getEndFrame_act() > 0) {
        int marginLeft =  round(( (a->getEndFrame_pre() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactorStart);
        int marginRight = round( (100 - ( (a->getEndFrame_act() / (double) opencvPlayer->getNumberOfFrames()) * 100)) * adjustFactorEnd);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: red; margin-left: %1%; margin-right: %2%}").arg(marginLeft).arg(marginRight);
        actionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::setPostActionalSliderStart(double adjustFactor, QSlider* postactionalSlider, Annotation* a) {
    if(a->getStartFrame_post() > 0) {
        int marginLeft =  round(( (a->getEndFrame_act() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactor);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: green; margin-left: %1%}").arg(marginLeft);
        postactionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::setPostActionalSliderEnd(double adjustFactorStart, double adjustFactorEnd, QSlider* postactionalSlider, Annotation* a) {
    if(a->getEndFrame_post() > 0) {
        int marginLeft =  round(( (a->getEndFrame_act() / (double) opencvPlayer->getNumberOfFrames()) * 100) * adjustFactorStart);
        int marginRight = round( (100 - ( (a->getEndFrame_post() / (double) opencvPlayer->getNumberOfFrames()) * 100)) * adjustFactorEnd);

        QString style = QString("QSlider::groove:horizontal {background: black;} QSlider::add-page:horizontal {background: green; margin-left: %1%; margin-right: %2%}").arg(marginLeft).arg(marginRight);
        postactionalSlider->setStyleSheet(style);
    }
}

void RBAnnotator::resizeSliders() {
    QVector<Annotation*> annotations = annotationWorker->getAnnotations();
    positionSlider->update();
    double adjustFactorStart = positionSlider->width() * SLIDER_ADJUST_RATIO_START;
    double adjustFactorEnd = positionSlider->width() * SLIDER_ADJUST_RATIO_END;

    for(int i=0; i<annotationPanesList.size(); i++) {
        QSlider * preactionalSlider = annotationPanesList[i]->findChild<QSlider*>("preactionalSlider");
        QSlider * actionalSlider = annotationPanesList[i]->findChild<QSlider*>("actionalSlider");
        QSlider * postactionalSlider = annotationPanesList[i]->findChild<QSlider*>("postactionalSlider");
        Annotation * a;

        if(i < annotations.size()) {
            a = annotations[i];
        } else {
            a = annotationWorker->getCurrentAnnotation();
        }

        // pre-actional slide
        setPreActionalSliderStart(adjustFactorStart, preactionalSlider, a);
        setPreActionalSliderEnd(adjustFactorStart, adjustFactorEnd, preactionalSlider, a);

        // actional slide
        setActionalSliderStart(adjustFactorStart, actionalSlider, a);
        setActionalSliderEnd(adjustFactorStart, adjustFactorEnd, actionalSlider, a);

        // post-actional slide
        setPostActionalSliderStart(adjustFactorStart, postactionalSlider, a);
        setPostActionalSliderEnd(adjustFactorStart, adjustFactorEnd, postactionalSlider, a);
    }
}

void RBAnnotator::closeEvent (QCloseEvent *event) {
    QMessageBox::StandardButton answer = QMessageBox::question( this, "", tr("Are you sure you want to quit?\n"
                                                                             "All non saved annotations will be lost!"), QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);

    if (answer != QMessageBox::Yes) {
        event->ignore();
    } else {
        workerThread->quit();
        progressDialog->close();
        delete progressDialog;
        event->accept();
    }
}

QWidget * RBAnnotator::createNewAnnotationPane(int n) {
    QBoxLayout * verticalLayout = new QVBoxLayout;
    QWidget * widget = new QWidget();

    QSlider * preactionalSlider = new QSlider(Qt::Horizontal);
    preactionalSlider->setEnabled(false);
    preactionalSlider->setRange(0, opencvPlayer->getNumberOfFrames());
    preactionalSlider->setObjectName("preactionalSlider");

    QSlider * actionalSlider = new QSlider(Qt::Horizontal);
    actionalSlider->setEnabled(false);
    actionalSlider->setRange(0, opencvPlayer->getNumberOfFrames());
    actionalSlider->setObjectName("actionalSlider");

    QSlider * postactionalSlider = new QSlider(Qt::Horizontal);
    postactionalSlider->setEnabled(false);
    postactionalSlider->setObjectName("postactionalSlider");
    postactionalSlider->setRange(0, opencvPlayer->getNumberOfFrames());

    preactionalSlider->setStyleSheet("QSlider::groove:horizontal {background: black;}");
    actionalSlider->setStyleSheet("QSlider::groove:horizontal {background: black;}");
    postactionalSlider->setStyleSheet("QSlider::groove:horizontal {background: black;}");

    QBoxLayout * horizontalLayout = new QHBoxLayout;

    QLineEdit * verbBox = new QLineEdit;
    verbBox->setPlaceholderText("Insert verb here");
    verbBox->setMaximumWidth(150);
    verbBox->setObjectName("verbBox");

    QLineEdit * objectBox = new QLineEdit;
    objectBox->setPlaceholderText("Insert object(s) here. Separate multiple obejcts with a comma");
    objectBox->setMinimumWidth(430);
    objectBox->setMaximumWidth(500);
    objectBox->setObjectName("objectBox");

    QLineEdit * preActionalLineEdit = new QLineEdit;
    preActionalLineEdit->setEnabled(false);
    preActionalLineEdit->setStyleSheet("color: blue; background-color: black; border: 0px");
    preActionalLineEdit->setMaximumWidth(300);
    preActionalLineEdit->setObjectName("preActionalLine");
    preActionalLineEdit->setAlignment(Qt::AlignRight);

    QLineEdit * actionalLineEdit = new QLineEdit;
    actionalLineEdit->setEnabled(false);
    actionalLineEdit->setStyleSheet("color: red; background-color: black; border: 0px");
    actionalLineEdit->setMaximumWidth(300);
    actionalLineEdit->setObjectName("actionalLine");
    actionalLineEdit->setAlignment(Qt::AlignRight);

    QLineEdit * postActionalLineEdit = new QLineEdit;
    postActionalLineEdit->setEnabled(false);
    postActionalLineEdit->setStyleSheet("color: green; background-color: black; border: 0px");
    postActionalLineEdit->setMaximumWidth(300);
    postActionalLineEdit->setObjectName("postActionalLine");
    postActionalLineEdit->setAlignment(Qt::AlignRight);

    QPushButton * deleteButton = new QPushButton("Delete annotation");
    connect(deleteButton, SIGNAL(clicked()), this, SLOT(deleteAnnotationClicked()));

    QPushButton * jumpButton = new QPushButton("Jump to");
    connect(jumpButton, SIGNAL(clicked()), this, SLOT(jumpToAnnotation()));

    horizontalLayout->addWidget(verbBox);
    horizontalLayout->addWidget(objectBox);
    horizontalLayout->addWidget(deleteButton);
    horizontalLayout->addWidget(jumpButton);
    horizontalLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    horizontalLayout->addWidget(new QLabel("Marked frames"));
    horizontalLayout->addWidget(preActionalLineEdit);
    horizontalLayout->addWidget(actionalLineEdit);
    horizontalLayout->addWidget(postActionalLineEdit);

    QFrame* hLine = new QFrame;
    hLine->setFrameShape(QFrame::HLine);

    verticalLayout->addWidget(hLine);
    verticalLayout->addLayout(horizontalLayout);
    verticalLayout->addWidget(preactionalSlider);
    verticalLayout->addWidget(actionalSlider);
    verticalLayout->addWidget(postactionalSlider);

    widget->setLayout(verticalLayout);

    return widget;
}

RBAnnotator::~RBAnnotator() {
    //delete opencvPlayer;
}

void RBAnnotator::newAnnotationClicked() {
    if(addAnnotationState == ADD_NEW_ANNOTATION) {
        newAnnotationButton->setText("Finalise annotation");
        newAnnotationButton->setEnabled(false);
        markButton->setEnabled(true);

        QWidget * annotationPane = createNewAnnotationPane(annotationWorker->getNumberOfAnnotations());
        annotationPane->setMaximumHeight(180);
        annotationsLayout->insertWidget(0, annotationPane);
        currentAnnotationPane = annotationPane;
        annotationPanesList.push_back(annotationPane);

        annotationWorker->newAnnotation();
        addAnnotationState = FINALISE_ANNOTATION;

        QLineEdit * verbBox = annotationPane->findChild<QLineEdit*>("verbBox");
        verbBox->setFocus();
    } else {
        finaliseAnnotation();
    }
}

void RBAnnotator::finaliseAnnotation() {
    QLineEdit * verbBox = currentAnnotationPane->findChild<QLineEdit*>("verbBox");
    QLineEdit * objectBox = currentAnnotationPane->findChild<QLineEdit*>("objectBox");

    if(verbBox->text().trimmed().isEmpty() || objectBox->text().trimmed().isEmpty()) {
        QMessageBox messageBox;
        messageBox.warning(0,"Warning","You must enter both a verb and at least one object!");
    } else {
        newAnnotationButton->setText("New annotation");
        //verbBox->setEnabled(false);
        //objectBox->setEnabled(false);
        exportButton->setEnabled(true);
        cutVideoButton->setEnabled(true);

        annotationWorker->setVerb(verbBox->text());
        annotationWorker->setObjects(objectBox->text());
        annotationWorker->setVideoName(currentVideoName);
        annotationWorker->finaliseAnnotation();

        addAnnotationState = ADD_NEW_ANNOTATION;
    }
}

void RBAnnotator::setVideoPlayer() {   
    QPixmap blackFrame;
    blackFrame.fill(Qt::black);
    videoFrame->setStyleSheet("background-color: black");
    videoFrame->setPixmap(blackFrame);

    opencvPlayer = new Player();
    QObject::connect(opencvPlayer, SIGNAL(processedImage(QImage)), this, SLOT(updatePlayerUI(QImage))); // we need to connect the player every time we create (load) a new (video) player
}

void RBAnnotator::enableMediaButtons(bool enable) {
    playButton->setEnabled(enable);
    oneFrameBackwardsButton->setEnabled(enable);
    oneFrameForwardsButton->setEnabled(enable);
    tenFramesBackwardsButton->setEnabled(enable);
    tenFramesForwardsButton->setEnabled(enable);
}

void RBAnnotator::getCutVideoSettings(bool *useOriginalCodec, int *frameWidth, int *frameHeight) {
    QLineEdit * frameWidthLineEdit = cutVideoSettingsDialog->findChild<QLineEdit*>("frameWidthLineEdit");
    QLineEdit * frameHeightLineEdit = cutVideoSettingsDialog->findChild<QLineEdit*>("frameHeightLineEdit");
    QCheckBox * useOriginalCodecCheckBox = cutVideoSettingsDialog->findChild<QCheckBox*>("formatCheckBox");

    *useOriginalCodec = useOriginalCodecCheckBox->isChecked();
    *frameWidth = frameWidthLineEdit->text().toInt();
    *frameHeight = frameHeightLineEdit->text().toInt();
}

void RBAnnotator::deleteAllAnnotations() {
    while(! annotationPanesList.empty()) {
        deleteAnnotation(annotationPanesList.back()); // this will delete annotations also in annotationsWorker
    }
}

void RBAnnotator::reset() {
    deleteAllAnnotations();

    delete opencvPlayer; // this will release all resources
    delete cutVideoSettingsDialog;

    setVideoPlayer();
    rotate180degrees = false;
    rotate180degreesCheckBox->setChecked(false);
    loadAnnotationsButton->setEnabled(false);
    enableMediaButtons(false);
    currentMarkingState = MARKING_PRE_START;
    addAnnotationState = ADD_NEW_ANNOTATION;
}

void RBAnnotator::markButtonClicked() {
    QSlider * slider;
    QLineEdit * lineEdit;
    QMessageBox messageBox;
    int start = opencvPlayer->getCurrentFramePosition();
    int end = start;
    double adjustFactorStart = positionSlider->width() * SLIDER_ADJUST_RATIO_START;
    double adjustFactorEnd = positionSlider->width() * SLIDER_ADJUST_RATIO_END;

    switch(currentMarkingState) {
    case MARKING_PRE_START:
        annotationWorker->setPreActionalStart(start);
        slider = currentAnnotationPane->findChild<QSlider*>("preactionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("preActionalLine");
        setPreActionalSliderStart(adjustFactorStart, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-").arg(start));
        markButton->setText("End pre-actional");
        currentMarkingState = MARKING_PRE_END;
        oneFrameForwards();
        break;
    case MARKING_PRE_END:
        if(end <= annotationWorker->getPreActionalStart()) {
            messageBox.critical(0,"Warning","Marked end must be ahead of marked start!");
            return;
        }

        annotationWorker->setPreActionalEnd(end);
        slider = currentAnnotationPane->findChild<QSlider*>("preactionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("preActionalLine");
        setPreActionalSliderEnd(adjustFactorStart, adjustFactorEnd, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-%2").arg(annotationWorker->getPreActionalStart()).arg(annotationWorker->getPreActionalEnd()));
        markButton->setText("Start actional");
        markButton->setStyleSheet("color: red;");
        currentMarkingState = MARKING_ACT_START;
        oneFrameForwards();
        markButtonClicked(); // marking directly the start of the actional phase as the following frame
        break;
    case MARKING_ACT_START:
        if(start <= annotationWorker->getPreActionalEnd()) {
            messageBox.critical(0,"Warning","The actional phase must start at least one frame after the end of the pre-actional phase!");
            return;
        }

        annotationWorker->setActionalStart(start);
        slider = currentAnnotationPane->findChild<QSlider*>("actionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("actionalLine");
        setActionalSliderStart(adjustFactorStart, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-").arg(start));
        markButton->setText("End actional");
        currentMarkingState = MARKING_ACT_END;
        oneFrameForwards();
        break;
    case MARKING_ACT_END:
        if(end <= annotationWorker->getActionalStart()) {
            messageBox.critical(0,"Warning","Marked end must be ahead of marked start!");
            return;
        }

        annotationWorker->setActionalEnd(end);
        slider = currentAnnotationPane->findChild<QSlider*>("actionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("actionalLine");
        setActionalSliderEnd(adjustFactorStart, adjustFactorEnd, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-%2").arg(annotationWorker->getActionalStart()).arg(annotationWorker->getActionalEnd()));
        markButton->setText("Start post-actional");
        markButton->setStyleSheet("color: green;");
        currentMarkingState = MARKING_POST_START;
        oneFrameForwards();
        markButtonClicked(); // marking directly the start of the post-actional phase as the following frame
        break;
    case MARKING_POST_START:
        if(start <= annotationWorker->getActionalEnd()) {
            messageBox.critical(0,"Warning","The post-actional phase must start at least one frame after the end of the actional phase!");
            return;
        }

        annotationWorker->setPostActionalStart(start);
        slider = currentAnnotationPane->findChild<QSlider*>("postactionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("postActionalLine");
        setPostActionalSliderStart(adjustFactorStart, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-").arg(start));
        markButton->setText("End post-actional");
        currentMarkingState = MARKING_POST_END;
        oneFrameForwards();
        break;
    case MARKING_POST_END:
        if(end <= annotationWorker->getPostActionalStart()) {
            messageBox.critical(0,"Warning","Marked end must be ahead of marked start!");
            return;
        }

        annotationWorker->setPostActionalEnd(end);
        slider = currentAnnotationPane->findChild<QSlider*>("postactionalSlider");
        lineEdit = currentAnnotationPane->findChild<QLineEdit*>("postActionalLine");
        setPostActionalSliderEnd(adjustFactorStart, adjustFactorEnd, slider, annotationWorker->getCurrentAnnotation());
        lineEdit->setText(QString("%1-%2").arg(annotationWorker->getPostActionalStart()).arg(annotationWorker->getPostActionalEnd()));
        markButton->setText("Start pre-actional");
        markButton->setStyleSheet("color: blue;");
        currentMarkingState = MARKING_PRE_START;
        oneFrameForwards();
        markButton->setEnabled(false);
        newAnnotationButton->setText("Finalise annotation");
        newAnnotationButton->setEnabled(true);
        break;
    }

    videoFrame->setFocus();
}

void RBAnnotator::jumpToAnnotation() {
    QObject * sender = this->sender(); // this is the button calling the slot
    QWidget * annotationPane = ((QWidget*) sender)->parentWidget(); //this is the annotation pane that will be deleted
    int index = annotationPanesList.indexOf(annotationPane); // annotations and annotation panes have the same indexes

    if(index < annotationWorker->getNumberOfAnnotations()) {
        Annotation * a = annotationWorker->getAnnotations().at(index);
        int t = a->getStartFrame_pre();

        if(t >= 0) {
            positionSlider->setValue(t);
            emit positionChanged(t); // this is needed
        }
    }
}

void RBAnnotator::loadAnnotationsClicked() {
    if(annotationWorker->getNumberOfAnnotations() > 0) {
        QMessageBox::StandardButton answer = QMessageBox::question( this, "",
                                                                    tr("Are you sure you want to load annotations?\nYou will loose current annotations!"),
                                                                    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
        if (answer != QMessageBox::Yes) {
            return;
        }
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open annotations text file"),QDir::homePath());

    if (!fileName.isEmpty()) {
        QFile file(fileName);

        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0, "Error", "Could not read file!");
            return;
        }

        QVector<int> badAnnotationsIdx;
        QVector<Annotation*> loadedAnnotations = Annotation::loadAnnotationsFromFile(file, badAnnotationsIdx);
        QVector<Annotation *> toBeRemoved;
        QStringList badList;

        // removing bad annotations
        for(int i=0; i<loadedAnnotations.size(); i++) {
            if(! isAnnotationValid(loadedAnnotations[i])) {
                badAnnotationsIdx.push_back(i);
                toBeRemoved.push_back(loadedAnnotations[i]);
            }
        }

        qSort(badAnnotationsIdx);

        for(int i=0; i<badAnnotationsIdx.size(); i++) {
            badList.push_back(QString("%1").arg(badAnnotationsIdx[i]+1));
        }

        if(badAnnotationsIdx.size() > 0) {

            for(int i=0; i<toBeRemoved.size(); i++) {
                loadedAnnotations.removeAll(toBeRemoved[i]);
            }

            QMessageBox::information(0, "Warning", "The following annotations were not valid and therefore not loaded: " + badList.join(","));
        }

        qSort(loadedAnnotations.begin(), loadedAnnotations.end(), Annotation::compareAnnotations);

        if(loadedAnnotations.size() > 0) {
            annotationWorker->stopAnnotating();
            deleteAllAnnotations();
            currentMarkingState = MARKING_PRE_START;
            addAnnotationState = ADD_NEW_ANNOTATION;
            newAnnotationButton->setText("New annotation");
            newAnnotationButton->setEnabled(true);
            cutVideoButton->setEnabled(true);
            exportButton->setEnabled(true);

            annotationWorker->setAnnotations(loadedAnnotations);
            annotationPanesList.resize(loadedAnnotations.size());

            for(int i=loadedAnnotations.size()-1; i>=0; i--) {
                QWidget * annotationPane = createNewAnnotationPane(annotationWorker->getNumberOfAnnotations());
                QLineEdit * verbBox = annotationPane->findChild<QLineEdit*>("verbBox");
                QLineEdit * objectBox = annotationPane->findChild<QLineEdit*>("objectBox");
                QLineEdit * lineEditPre = annotationPane->findChild<QLineEdit*>("preActionalLine");
                QLineEdit * lineEditAct = annotationPane->findChild<QLineEdit*>("actionalLine");
                QLineEdit * lineEditPost = annotationPane->findChild<QLineEdit*>("postActionalLine");

                lineEditPre->setText(QString("%1-%2").arg(loadedAnnotations[i]->getStartFrame_pre()).arg(loadedAnnotations[i]->getEndFrame_pre()));
                lineEditAct->setText(QString("%1-%2").arg(loadedAnnotations[i]->getStartFrame_act()).arg(loadedAnnotations[i]->getEndFrame_act()));
                lineEditPost->setText(QString("%1-%2").arg(loadedAnnotations[i]->getStartFrame_post()).arg(loadedAnnotations[i]->getEndFrame_post()));
                verbBox->setText(loadedAnnotations[i]->getVerb());
                objectBox->setText(loadedAnnotations[i]->getObjects().join(","));

                annotationPane->setMaximumHeight(180);
                annotationsLayout->insertWidget(0, annotationPane);
                annotationPanesList[i] = annotationPane;

                loadedAnnotations[i]->setVideoName(currentVideoName);
            }

            currentAnnotationPane = annotationPanesList.back();
            resizeSliders(); // this will set the annotation panes sliders
        } else {
            QMessageBox messageBox;
            messageBox.warning(0,"Warning", "Could not load any annotation! Please check the file!");
        }
    }
}

bool RBAnnotator::updateAnnotations() {
    QVector<QPair<QString, QString > > verbsAndObjects;

    for(int i=0; i<annotationPanesList.size(); i++) {
        QLineEdit * verbBox = annotationPanesList[i]->findChild<QLineEdit*>("verbBox");
        QLineEdit * objectBox = annotationPanesList[i]->findChild<QLineEdit*>("objectBox");

        if(verbBox->text().trimmed().isEmpty() || objectBox->text().trimmed().isEmpty()) {
            QMessageBox messageBox;
            messageBox.warning(0,"Warning",QString("Annotation %1 has empty verb and/or objects!\nPlease fix it and try again!").arg(i));
            return false;
        }

        QPair<QString, QString> pair(verbBox->text(), objectBox->text());
        verbsAndObjects.push_back(pair);
    }

    // if we arrive here we are sure no annotation is invalid
    annotationWorker->updateVerbsAndObjects(verbsAndObjects);

    return true;
}

bool RBAnnotator::isAnnotationValid(const Annotation *a) {
    int n = opencvPlayer->getNumberOfFrames();

    if(a->getStartFrame_pre() < 0 || a->getStartFrame_pre() > n)
        return false;
    if(a->getEndFrame_pre() < 0 || a->getEndFrame_pre() > n)
        return false;
    if(a->getEndFrame_pre() <= a->getStartFrame_pre())
        return false;

    if(a->getStartFrame_act() < 0 || a->getStartFrame_act() > n)
        return false;
    if(a->getEndFrame_act() < 0 || a->getEndFrame_act() > n)
        return false;
    if(a->getEndFrame_act() <= a->getStartFrame_act())
        return false;

    if(a->getStartFrame_post() < 0 || a->getStartFrame_post() > n)
        return false;
    if(a->getEndFrame_post() < 0 || a->getEndFrame_post() > n)
        return false;
    if(a->getEndFrame_post() <= a->getStartFrame_post())
        return false;

    if( !(a->getEndFrame_pre() < a->getStartFrame_act()) || !(a->getEndFrame_act() < a->getStartFrame_post()))
        return false;

    return true;
}

bool RBAnnotator::exportAnnotationTextFile(QString outputFile) {
    QFileInfo fileInfo = QFileInfo(outputFile);

    bool updateOk = updateAnnotations();

    if(! updateOk)
        return false;

    if(fileInfo.exists()) {
        bool ok;

        QString text = QInputDialog::getText(this,
                                             "File already exists",
                                             "An annotation file already exists in the folder!\nSpecify a new name or press ok to merge annotations files", QLineEdit::Normal,
                                             fileInfo.fileName(), &ok);
        if (ok && !text.isEmpty()) {
            outputFile = fileInfo.absolutePath() + QDir::separator() + text;
        } else if(!ok)
            return false;
    }

    QFile f(outputFile);

    return Annotation::exportAnnotationsToFile(f, annotationWorker->getAnnotations());
}

void RBAnnotator::exportAnnotationsClicked() {
    QString fileName = annotationWorker->getAnnotations().front()->getVideoName() + "_rbAnnotations.txt";
    QString outputFile = QFileDialog::getSaveFileName(this, tr("Select output file"),QDir::homePath() + QDir::separator() + fileName);

    if (!outputFile.isEmpty()) {
        bool ok = exportAnnotationTextFile(outputFile); // this will also update the annotations
        QMessageBox messageBox;

        if(ok)
            messageBox.information(0,"","Annotations successfully exported!");
        else
            messageBox.warning(0,"Error","Could not export annotations!");
    }

    videoFrame->setFocus();
}

void RBAnnotator::cutVideo() {
    cutVideoSettingsDialog->exec();

    QString outputFolder = QFileDialog::getExistingDirectory(this, tr("Select output folder"),QDir::homePath());

    if (!outputFolder.isEmpty()) {
        outputFolder += QDir::separator() + annotationWorker->getAnnotations().front()->getVideoName() + "_rbAnnotations";

        if(! QDir(outputFolder).exists())
            QDir().mkdir(outputFolder);

        // exporting annotations as text file as well
        QString annotationsTextFile = outputFolder + QDir::separator() + annotationWorker->getAnnotations().front()->getVideoName() + "_rbAnnotations.txt";
        bool ok = exportAnnotationTextFile(annotationsTextFile); // this will also update the annotations
        QMessageBox messageBox;

        if(!ok) {
            messageBox.warning(0,"Error","Could not export annotations!");
            return;
        }

        bool useOriginalCodec = false;
        int frameWidth = 0, originalFrameWidth;
        int frameHeight = 0, originalFrameHeight;

        getCutVideoSettings(&useOriginalCodec, &frameWidth, &frameHeight);
        opencvPlayer->getFrameSize(&originalFrameWidth, &originalFrameHeight);

        if(frameWidth <= 0)
            frameWidth = originalFrameWidth;

        if(frameHeight <=0)
            frameHeight = originalFrameHeight;

        bool resize = (frameWidth != originalFrameWidth) || (frameHeight != originalFrameHeight);

        // cutting video in a separate thread
        emit cutVideo(opencvPlayer->getCapture(), outputFolder, useOriginalCodec, resize, frameWidth, frameHeight);
        progressDialog->reset("Cutting video", annotationWorker->getNumberOfAnnotations()*6); // every annotations will generate 6 videos
        progressDialog->show();
    }

    videoFrame->setFocus();
}

void RBAnnotator::currentFrameEdited() {
    int frame = currentFrameLineEdit->text().toInt();

    if(frame <= opencvPlayer->getNumberOfFrames()) {
        positionSlider->setValue(frame);
        emit positionChanged(frame); // this is needed
    }

    videoFrame->setFocus();
}

void RBAnnotator::currentTimeEdited() {
    QTime t = QTime::fromString(currentTimeLineEdit->text(), "hh:mm:ss:zzz");

    if(t.isValid()) {
        int ms = QTime(0,0).msecsTo(t);

        if(ms < opencvPlayer->getLengthMs()) {
            opencvPlayer->setCurrentMs(ms);
            int frame = opencvPlayer->getCurrentFramePosition();
            positionSlider->setValue(frame);
            emit positionChanged(frame); // this is needed
        }
    }

    videoFrame->setFocus();
}

void RBAnnotator::deleteAnnotation(QWidget* toBeDeletedPane) {
    int index = annotationPanesList.indexOf(toBeDeletedPane); // annotations and annotation panes have the same indexes
    annotationPanesList.remove(index);

    /*
    // reordering annotation panes
    for(int i=0; i<annotationsLayout->children().size(); i++)
        annotationsLayout->takeAt(i);

    for(int i=annotationPanesList.size()-1; i>0; i--)
        annotationsLayout->addWidget(annotationPanesList[i]);
    */

    if(toBeDeletedPane == currentAnnotationPane) {
        annotationWorker->stopAnnotating();
    }

    delete toBeDeletedPane; // only this will actualy delete the pane from the screen
    annotationsLayout->update();

    if(! annotationWorker->isAnnotating()) {
        newAnnotationButton->setEnabled(true);
        newAnnotationButton->setText("Add new annotation");
        markButton->setEnabled(false);
        markButton->setText("Start pre-actional");
        markButton->setStyleSheet("color: blue;");
        addAnnotationState = ADD_NEW_ANNOTATION;
        currentMarkingState = MARKING_PRE_START;
    }

    if(annotationPanesList.size() == 0) {
        exportButton->setEnabled(false);
        cutVideoButton->setEnabled(false);
    }

    annotationWorker->deleteAnnotation(index);
}

void RBAnnotator::deleteAnnotationClicked() {
    QMessageBox::StandardButton reply;

    reply = QMessageBox::question(this, "Confirm delete", "Are you sure you want to delete this annotation?", QMessageBox::Yes|QMessageBox::No);

    if(reply == QMessageBox::No)
        return;

    QObject * sender = this->sender(); // this is the button calling the slot
    QWidget * toBeDeletedPane = ((QWidget*) sender)->parentWidget(); //this is the annotation pane that will be deleted

    deleteAnnotation(toBeDeletedPane);
    videoFrame->setFocus();
}

void RBAnnotator::userStartedMovingSlider() {
    this->userMovingSlider = true;

    if(! opencvPlayer->isStopped()) {
        opencvPlayer->stopVideo();
        videoWasPlayingBeforeMovingSlider = true;
    }
}

void RBAnnotator::userStoppedMovingSlider() {
    this->userMovingSlider = false;

    if(videoWasPlayingBeforeMovingSlider)
        opencvPlayer->playVideo();

    videoWasPlayingBeforeMovingSlider = false;
    videoFrame->setFocus();
}

void RBAnnotator::loadVideo() {
    if(opencvPlayer->isVideoLoaded()) {
        QMessageBox::StandardButton answer = QMessageBox::question( this, "",
                                                                    tr("Are you sure you want to open a new video?\nAll non saved annotations will be lost!\n"),
                                                                    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);

        if (answer != QMessageBox::Yes) {
            return;
        } else {
            reset();
        }
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Movie"),QDir::homePath());

    if (!fileName.isEmpty()) {
        QFileInfo fileInfo(fileName);

        opencvPlayer->loadVideo(fileName);
        currentVideoName = fileInfo.baseName();

        if (!opencvPlayer->isVideoLoaded()) {
            QMessageBox messageBox;
            messageBox.critical(0,"Error","Could not open video!");
            return;
        }

        enableMediaButtons(true);
        newAnnotationButton->setEnabled(true);
        currentFrameLineEdit->setEnabled(true);
        currentTimeLineEdit->setEnabled(true);
        loadAnnotationsButton->setEnabled(true);
        videoNameLabel->setText("Annotating video:  " + fileInfo.absoluteFilePath());
        positionSlider->setRange(1, opencvPlayer->getNumberOfFrames());
        cutVideoSettingsDialog = createCutVideoSettingsDialog();
        videoFrame->setFocus();
        positionSlider->setValue(1);
    }
}

void RBAnnotator::play() {
    if(! opencvPlayer->isVideoLoaded())
        return;

    if(opencvPlayer->isStopped()) {
        opencvPlayer->playVideo();
        playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    } else {
        opencvPlayer->stopVideo();
        playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    }
}

void RBAnnotator::positionChanged(int position) {
    currentFrameLineEdit->setText(QString("%1").arg(position));

    int ms = opencvPlayer->getCurrentMsPosition();

    QTime t(0,0);
    t = t.addMSecs(ms);
    currentTimeLineEdit->setText(t.toString("hh:mm:ss:zzz"));

    if(! userMovingSlider && opencvPlayer->isStopped()) {
        opencvPlayer->setCurrentFrame(position);
    }
}

void RBAnnotator::setPosition(int position) {
    opencvPlayer->setCurrentFrame(position);
}

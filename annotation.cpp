/*
 * RBAnnotator
 *
 * Author: Davide Moltisanti
 * Code released under GPL license
 *
*/

#include "annotation.h"

Annotation::Annotation() {
    startFrame_pre = -1;
    startFrame_act = -1;
    startFrame_post = -1;
    endFrame_pre = -1;
    endFrame_act = -1;
    endFrame_post = -1;
}

int Annotation::getStartFrame_pre() const {
    return startFrame_pre;
}

void Annotation::setStartFrame_pre(int value) {
    startFrame_pre = value;
}

int Annotation::getEndFrame_pre() const {
    return endFrame_pre;
}

void Annotation::setEndFrame_pre(int value) {
    endFrame_pre = value;
}

int Annotation::getStartFrame_act() const {
    return startFrame_act;
}

void Annotation::setStartFrame_act(int value) {
    startFrame_act = value;
}

int Annotation::getEndFrame_act() const {
    return endFrame_act;
}

void Annotation::setEndFrame_act(int value) {
    endFrame_act = value;
}

int Annotation::getStartFrame_post() const {
    return startFrame_post;
}

void Annotation::setStartFrame_post(int value) {
    startFrame_post = value;
}

int Annotation::getEndFrame_post() const {
    return endFrame_post;
}

void Annotation::setEndFrame_post(int value) {
    endFrame_post = value;
}

QString Annotation::getVerb() {
    return verb;
}

void Annotation::setVerb(QString value) {
    verb = QString(value.trimmed().toLower());
    verb.replace(" ", "-");
}

QStringList Annotation::getObjects() const {
    return objects;
}

void Annotation::setObjects(const QStringList &value) {
    objects = value;
}

void Annotation::setObjects(QString value) {
    QStringList split = value.split(",");
    objects.clear();

    for(int i=0; i<split.size(); i++) {
        QString s = split.at(i);
        s = s.trimmed().toLower();
        s.replace(" ", "-");
        objects << s;
    }

    qSort(objects.begin(), objects.end());
}

QString Annotation::getVideoName() const {
    return videoName;
}

void Annotation::setVideoName(const QString &value) {
    videoName = value;
}

QString Annotation::objectsToString() {
    return objects.join("+");
}

QString Annotation::formatAnnotation() {
    return
            verb + "_" + objectsToString() + "_" +
            QString::number(startFrame_pre) + "-" + QString::number(endFrame_pre) + "_" + QString::number(startFrame_act) + "-" + QString::number(endFrame_act) + "_" +
            QString::number(startFrame_post) + "-" + QString::number(endFrame_post);
}

QString Annotation::buildFileName(int whichOne) {
    QString base(videoName + "_" + verb + "_" + objectsToString() + "_");


    // sub-phases are supposed to be contiguos
    switch(whichOne) {
    case ALL:
        base += QString::number(startFrame_pre) + "-" + QString::number(endFrame_post) + "_all";
        break;
    case PRE:
        base += QString::number(startFrame_pre) + "-" + QString::number(endFrame_pre) + "_pre";
        break;
    case ACT:
        base +=  QString::number(startFrame_act) + "-" + QString::number(endFrame_act) + "_act";
        break;
    case POST:
        base += QString::number(startFrame_post) + "-" + QString::number(endFrame_post) + "_post";
        break;
    case PRE_ACT:
        base += QString::number(startFrame_pre) + "-" + QString::number(endFrame_act) + "_pre-act";
        break;
    case ACT_POST:
        base += QString::number(startFrame_act) + "-" + QString::number(endFrame_post) + "_act-post" ;
        break;
    }

    return base;
}

QVector<Annotation *> Annotation::loadAnnotationsFromFile(QFile &annotationFile, QVector<int> &badAnnotationsIdx) {
    // file has been already opened
    QVector<Annotation *> annotations;
    QTextStream in(&annotationFile);
    int i =-1;

    while(!in.atEnd()) {
        try {
            QString line = in.readLine();
            Annotation * a = parseAnnotationFromString(line);
            i++;

            if(a == NULL) {
                badAnnotationsIdx.push_back(i);
                continue;
            }

            annotations.push_back(a);
        } catch(std::exception &e) {
            continue;
        }
    }

    annotationFile.close();
    return annotations;
}

Annotation *Annotation::parseAnnotationFromString(QString &text) {
    QStringList split = text.split("_");

    if(split.size() != 5)
        return NULL;

    QStringList objects = split[1].split("+");
    QStringList preFrames = split[2].split("-");

    if(preFrames.size() != 2)
        return NULL;

    QStringList actFrames = split[3].split("-");

    if(actFrames.size() != 2)
        return NULL;

    QStringList postFrames = split[4].split("-");

    if(postFrames.size() != 2)
        return NULL;

    Annotation * a = new Annotation;

    a->setVerb(split[0]);
    a->setObjects(objects);
    a->setStartFrame_pre(preFrames[0].toInt());
    a->setEndFrame_pre(preFrames[1].toInt());
    a->setStartFrame_act(actFrames[0].toInt());
    a->setEndFrame_act(actFrames[1].toInt());
    a->setStartFrame_post(postFrames[0].toInt());
    a->setEndFrame_post(postFrames[1].toInt());

    return a;
}

bool Annotation::exportAnnotationsToFile(QFile &outputFile, QVector<Annotation *> annotations) {
    if(outputFile.open(QFile::ReadWrite | QIODevice::Append)) {
        QTextStream out(&outputFile);
        qSort(annotations.begin(), annotations.end(), compareAnnotations);

        for(int i=0; i<annotations.size(); i++)
            out << annotations[i]->formatAnnotation() << "\n";

        return true;
    } else {
        return false;
    }
}
